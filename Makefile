REPO = github.com/hashicorp/terraform
SRC_PATH = $(GOPATH)/src/$(REPO)

XC_OS ?= $(shell go env GOOS)
XC_ARCH ?= $(shell go env GOARCH)

default: build

prepare:
	mkdir -p $(shell dirname $(SRC_PATH))
	cp -a terraform/ $(SRC_PATH)/
	rm -rf bin

build: prepare
	echo "GOsrc = $$SRC_PATH"
	make bin XC_ARCH=$(XC_ARCH) XC_OS=$(XC_OS) -C $(SRC_PATH)
	cp -a $(SRC_PATH)/bin ./

.PHONY: default build prepare
